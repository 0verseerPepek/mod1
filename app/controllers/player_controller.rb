class PlayerController < ApplicationController

   def show_one
   	@Play = Player.find(para[:nickname])
   end
   def show
   	@Play = Player.all
   end
   
   def new
   	@Play = Player.new
   end
   
   def create
   	@Play = Player.new(para)

   	if @Play.save
   	redirect_to root_path
   else 
   	render 'new'
   end
   
   def edit
   	@Play = Player.find(para[:nickname])
   	if @Play.update(para)
    redirect_to root_path
  else
    render 'edit'
   end
   
   def delete
   	@Play = Player.find(para[:nickname])
   	@Play.delete()
   	redirect_to root_path
   end
   	private def para
   	params.require(:player).permit(:nickname,:rank, :charisma, :wisdom)
   end
end
